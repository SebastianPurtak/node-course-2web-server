const express = require('express');
const hbs = require('hbs');
const fs = require('fs');

const port = process.env.PORT || 3000;      
var app = express();

hbs.registerPartials(__dirname + '/views/partials');   
app.set('view engine', 'hbs');       

app.use((req, res, next) => {
    var now = new Date().toString();
    var log = `${now}: ${req.method} ${req.url}`;

    console.log(log);
    fs.appendFile('server.log', log + '\n', (err) => {
        console.log('Unable to append to server.log.');
    });
    next();     
});

// app.use((req, res, next) => {
//     res.render('maintance.hbs', {
//         pageTitle: "We'll be right back.", 
//         maintanceText: "The site is beaing currently updateing, We'll be back soon."
//     });
// });

app.use(express.static(__dirname + '/public'));  

hbs.registerHelper('getCurrentYear', () => {        
    return new Date().getFullYear();
});

hbs.registerHelper('screamIt', (text) => {
    return text.toUpperCase();
});

app.get('/', (req, res) => {            
    res.render('home.hbs',{
        pageTitle: 'Home Page',
        welcomeText: 'Kocham mojego Kotka!:*',
    })
}); 

app.get('/about', (req, res) => {       
    res.render('about.hbs', {
        pageTitle: 'Abaut Page',
    });                   
});

app.get('/bad', (req, res) => {
    res.send({
        errorMessage: 'Unable to fulfill this request'
    });
});

app.get('/projects', (req, res) => {
    res.render('projects.hbs', {
        pageTitle: 'Projects',
        welcomeText: 'Portfolio page:'
    });          
});

app.listen(port, () => {
    console.log(`Serwer is up on port ${port}.`)
});      